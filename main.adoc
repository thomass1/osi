= Project Proposal
include::0_resources/preamble.adoc[]

include::subchapters/1-1_executive_summary.adoc[]

== Overview
include::subchapters/1-2_motivation.adoc[]
include::subchapters/1-3_use_cases.adoc[]
include::subchapters/1-4_requirements.adoc[]
include::subchapters/1-5_relations_to_other_standards_projects_or_organizations.adoc[]

== Technical Content
include::subchapters/2_technical-content.adoc[]

== Project Resources
include::subchapters/3-1_required_resources.adoc[]
include::subchapters/3-2_committed_resources.adoc[]
include::subchapters/3-3_resource_summary.adoc[]

== Project Plan
include::subchapters/3-4_timeline.adoc[]
include::subchapters/3-5_deliverables.adoc[]
include::subchapters/3-6_review_process.adoc[]
    
